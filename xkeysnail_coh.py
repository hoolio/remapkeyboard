import re
from xkeysnail.transform import *

# [Conditional modmap] Change modifier keys in certain applications
define_conditional_modmap(re.compile(r'steam_app_228200'), {

    # rebind WASD for map nav
    Key.W: Key.UP,
    Key.A: Key.LEFT,
    Key.S: Key.DOWN,
    Key.D: Key.RIGHT,
})

# # TESTING
# define_conditional_modmap(re.compile(r'Code'), {
#     # send 2 instead of 1 in VS Code
#     Key.KEY_1: Key.KEY_2,
# })