# how I use xkeysnail to remap keys for COH in linux

# install
```
sudo pip3 install xkeysnail
xhost +SI:localuser:root
``` 

more doco at https://github.com/mooz/xkeysnail

> eek! apparently thats not being maintained, switch to https://github.com/joshgoebel/keyszer?

# config
see config to understand the remaps being applied.

eg this rebinds W to UP arrow for the wm_class matching steam_app_228200
```
define_conditional_modmap(re.compile(r'steam_app_228200'), {
    Key.W: Key.UP,
})
```

# exec
```
sudo xkeysnail xkeysnail_coh.py
```

and you should see wm_class of the active window and any keybinds being applied.

# gotchas

how to get wm_class of COH when xprop + click is not working
```
hoolio@ryzen:~:$ wmctrl -xlp | grep Comp
0x05000001  3 140896 steam_app_228200.steam_app_228200  ryzen Company Of Heroes
hoolio@ryzen:~:$ xprop -id 0x05000001 WM_CLASS
WM_CLASS(STRING) = "steam_app_228200", "steam_app_228200"
```

# appendix, how i run COH in linux
1. install COH and DLC via Steam
1. install COH Blitzkreig mod via Steam
1. download, install and GE-Proton7-49 for COH
1. don't run the Blitzkreig mod launcher (it doesn't render), instead
1. start COH directly with the following launch option
```
PROTON_USE_D9VK=0 PROTON_NO_D3D11=1 PROTON_FORCE_LARGE_ADDRESS_AWARE=1 PROTON_NO_ESYNC=1 PROTON_NO_FSYNC=1 %command% -nomovies -dev -mod blitzkrieg -novsync -notriplebuffer -refresh 60
```
any issues have a look at https://www.protondb.com/app/681590
